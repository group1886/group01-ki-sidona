from decimal import Context
from django.http import request
from django.shortcuts import render, redirect
from django.db import connection
from .forms import WithdrawForm
from login.views import is_individual
from django.utils import timezone



def prelogin(request): 
    if 'email' in request.session:
        return redirect('/dashboard/')
    return render(request, 'main/prelogin.html')

def dashboard(request):
    if 'email' not in request.session:
        return redirect('/')
    if 'is_admin' in request.session:
        title = 'Admin Dashboard'
    elif 'is_individual' in request.session or 'is_organization' in request.session:
        title = 'User Dashboard'
    else:
        request.session.flush()
        return redirect('/')

    context = {
        "title" : title
    }
    return render(request, 'main/dashboard.html', context)

def personal_fundraising(request):
    if 'email' not in request.session:
        return redirect('/')

    if 'is_individual' not in request.session and 'is_organization' not in request.session:
        return redirect('/unauthorized/')

    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")
    cursor.execute("SELECT F.id fundid,title,city,province,initialactivedate,lastactivedate,remainingdays, \
                    amountoffundsneeded,categoryname,verificationstatus \
                    FROM FUNDRAISING F, CATEGORY C WHERE emailuser=%s AND F.categoryid = C.id;", 
                    [request.session['email']])
    fundraising = fetch(cursor)

    cursor.execute("SELECT numboffund, numb_of_active_fund FROM FUNDRAISER WHERE email=%s",[request.session['email']])
    amount = fetch(cursor)[0]

    context = {
        "fundraising" : fundraising,
        "amount" : amount
    }

    return render(request, 'main/personal_fundraising.html', context)

def profile(request):
    if 'email' not in request.session:
        return redirect('/')

    if 'is_admin' in request.session:
        return redirect('/unauthorized/')
    
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")

    cursor.execute("SELECT * FROM FUNDRAISER WHERE email=%s;", [request.session['email']])
    fundraiser_info = fetch(cursor)[0]


    context = {
        "fundraiser_info":fundraiser_info,
    }

    if 'is_individual' in request.session:
        cursor.execute("SELECT nik,birthdate,gender,numberofwishlist FROM INDIVIDUAL WHERE email=%s;", [request.session['email']])
        context["individual_info"] = fetch(cursor)[0]
        is_individual = True

        cursor.execute("SELECT W.fundid, F.title, C.categoryname \
                        FROM WISHLIST_DONATION W, FUNDRAISING F, CATEGORY C \
                        WHERE W.email=%s AND W.fundid=F.id AND F.categoryid=C.id;", [request.session['email']])
        
        wishlist = fetch(cursor)
        context['wishlist'] = wishlist

    else:
        cursor.execute("SELECT deednumber,organizationname,telpnumb,establishmentyear FROM ORGANIZATION WHERE email=%s;", [request.session['email']])
        context["organization_info"] = fetch(cursor)[0]
        is_individual = False
    
    context["is_individual"] = is_individual
    return render(request, 'main/fundraiser_profile.html', context)

def view_user(request, user):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")

    email = get_user(user)

    cursor.execute("SELECT * FROM FUNDRAISER WHERE email=%s;", [email])
    fundraiser_info = fetch(cursor)[0]

    context = {
        "fundraiser_info":fundraiser_info,
    }

    cursor.execute("SELECT email FROM INDIVIDUAL;")
    test = fetch(cursor)
    is_individual = False

    for e in test:
        if email == e['email']:
            is_individual = True

    if is_individual:
        cursor.execute("SELECT nik,birthdate,gender,numberofwishlist FROM INDIVIDUAL WHERE email=%s;", [email])
        context["individual_info"] = fetch(cursor)[0]
        

    else:
        cursor.execute("SELECT deednumber,organizationname,telpnumb,establishmentyear FROM ORGANIZATION WHERE email=%s;", [email])
        context["organization_info"] = fetch(cursor)[0]

    context["user"] = user
    context["is_individual"] = is_individual

    return render(request, 'main/profile_admin_view.html', context)

def users_list(request):

    context = dict()

    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")

    query = """
            select f.email, f.name, 'Individual' type, f.verificationstatus from fundraiser f, individual i, organization o
            where f.email = i.email
            union
            select f.email, f.name, 'Organization' type, f.verificationstatus from fundraiser f, individual i, organization o
            where f.email = o.email;
            """

    cursor.execute(query)
    users = fetch(cursor)

    for user in users:
        username = ''
        for i in user['email']:
            if i != "@":
                username += i
            else:
                break       
        user['username'] = username

    context["users"] = users
    return render(request, 'main/users_list.html', context)

def verify_user(request, user):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")

    email = get_user(user)
    cursor.execute("UPDATE FUNDRAISER SET verificationstatus = 'Sudah Terverifikasi', emailadmin=%s WHERE email=%s;",
                     [request.session['email'], email])

    return redirect('/user/list/')

def withdraw_request(request, fundid):
    #print(fundid)
    context = {}
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")
    cursor.execute('select f.donapaysaldo, fu.title from fundraiser as f, fundraising as fu where f.email=%s and f.email=fu.emailuser'
    ,[request.session['email']])
    saldo_title = fetch(cursor)
    error_message = ""
    

    if request.method == 'POST':
        nominal =  request.POST['nominal']
        description =  request.POST['description']
        print(nominal)
        try:
            float(nominal)
        except:
            error_message = "Amount not valid."
            
        else:
            try:
                cursor.execute("INSERT INTO HISTORY_OF_FUND_USE (fundid,timestamp,nominalused,description) \
                        values (%s,%s,%s,%s)", [fundid,timezone.now(),nominal,description])
            except:
                error_message = "Not enough credit"
    context["saldo_title"] = saldo_title
    context["error"] = error_message
    

    return render(request, 'main/withdraw_request.html', context)


    

def fetch(cursor):
    column = [col[0] for col in cursor.description]     # cursor.description is for the names of the columns
    return [dict(zip(column, row)) for row in cursor.fetchall()]  # fetchall() to get the values of the table, result -> {'email':e, ...}

def get_user(user):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")

    query = """
            select f.email, f.name, 'Individual' type, f.verificationstatus from fundraiser f, individual i, organization o
            where f.email = i.email
            union
            select f.email, f.name, 'Organization' type, f.verificationstatus from fundraiser f, individual i, organization o
            where f.email = o.email;
            """

    cursor.execute(query)
    users = fetch(cursor)

    for u in users:
        username = ''
        for i in u['email']:
            if i != "@":
                username += i
            else:
                break       
        if user == username:
            email = u['email']
            break

    return email