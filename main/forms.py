from django import forms
from django.forms.fields import CharField, FloatField

class WithdrawForm(forms.Form):
    nominal = forms.FloatField()
    description = CharField(max_length=50)
    
