from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.prelogin, name='home'),
    path('dashboard/', views.dashboard, name='admin_dashboard'),
    path('personal-fundraising/', views.personal_fundraising, name='personal_fundraising'),
    path('profile/', views.profile, name='profile'),
    path('view-user/<str:user>/', views.view_user, name='view_user'),
    path('verify-user/<str:user>/', views.verify_user, name='verify_user'),
    path('user/list/', views.users_list, name='users_list'),
    path('personal-fundraising/withdraw/<str:fundid>', views.withdraw_request, name='withdraw_request'),
]
