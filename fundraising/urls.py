from django.urls import path

from . import views

app_name = 'fundraising'

urlpatterns = [

    path('page/', views.fundraising_list_admin, name='fundraising_list_admin'),
    path('list/', views.fundraising_list_user, name='fundraising_list_user'),
    
    path('verification-form/<str:fund_id>/', views.fundraising_verification_form, name='fundraising_verification_form'),
    path('verify/<str:fund_id>/', views.verify_fundraising, name='verify_fundraising'),
    path('details/<str:fund_id>/', views.fundraising_details, name='fundraising_details'),
    path('update/<str:fund_id>/', views.fundraising_update, name='fundraising_update'),
    path('delete/<str:fund_id>/', views.fundraising_delete, name='fundraising_delete'),

    path('manage/', views.Manage_comorbid_diseases, name='Manage_comorbid_diseases'),
    path('manage/form/', views.comorbid_disease_form, name='views.comorbid_disease_form'),
    path('manage/update/', views.comorbid_disease_update, name='views.comorbis_disease_update'),
]

