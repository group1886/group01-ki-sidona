from django.db import connection
from django.forms.fields import ChoiceField
from django.utils import timezone
from django.shortcuts import redirect, render

from create_fundraising.forms import HouseOfWorshipForm
from .forms import ComorbidForm, ComorbidUpdateForm, FundraisingUpdateForm, FundraisingVerificationForm, HealthUpdateForm, HoWUpdateForm


def fundraising_list_admin(request):
    if 'email' not in request.session:
        return redirect('/')

    if 'is_admin' not in request.session:
        return redirect('/unauthorized/')

    context = dict()

    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")

    cursor.execute("SELECT F.id fundid,title,city,province,initialactivedate,lastactivedate,remainingdays, \
                    amountoffundsneeded,categoryname,verificationstatus \
                    FROM FUNDRAISING F, CATEGORY C WHERE F.categoryid = C.id;")    
    fundraising = fetch(cursor)

    context['fundraising'] = fundraising

    return render(request, 'fundraising_list_admin.html', context)

def fundraising_list_user(request):
    if 'email' not in request.session:
        return redirect('/')

    if 'is_admin' in request.session:
        return redirect('/unauthorized/')

    context = dict()

    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")

    cursor.execute("SELECT F.id fundid,title,city,province,lastactivedate,remainingdays, amountoffundsneeded,categoryname \
                    FROM FUNDRAISING F, CATEGORY C WHERE F.categoryid = C.id \
                    AND verificationstatus='Sudah Terverifikasi';")    
    fundraising = fetch(cursor)

    context['fundraising'] = fundraising

    return render(request, 'fundraising_list_user.html', context)

def fundraising_verification_form(request, fund_id):
    if 'email' not in request.session:
        return redirect('/')

    if 'is_admin' not in request.session:
        return redirect('/unauthorized/')

    error = ''
    form = FundraisingVerificationForm(request.POST or None)

    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")  

    if form.is_valid() and request.POST:
        if request.POST['verdict'] == 'Reject':
            if request.POST['note'] == '':
                error = "Please write a submission note for the fundraiser"
            else:
                cursor.execute("SELECT information FROM SUBMISSION_NOTES where fundid=%s;",[fund_id])
                if fetch(cursor) != []:
                    cursor.execute("UPDATE SUBMISSION_NOTES SET information=%s WHERE fundid=%s;",[request.POST['note'], fund_id], )
                else:
                    cursor.execute("INSERT INTO SUBMISSION_NOTES (fundid,timestamp,information) values (%s,%s,%s)",[fund_id, timezone.now(), request.POST['note']] )

                return redirect('/fundraising/page/')
        elif request.POST['verdict'] == 'Approve':
            return redirect(f'/fundraising/verify/{fund_id}/')
     

    context = {
        "form":form,
        "error":error
    }
    return render(request, 'fundraising_verification_form.html',context)

def verify_fundraising(request, fund_id):
    if 'email' not in request.session:
        return redirect('/')

    if 'is_admin' not in request.session:
        return redirect('/unauthorized/')

    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")

    cursor.execute("UPDATE FUNDRAISING SET verificationstatus='Sudah Terverifikasi' \
                    WHERE id=%s;", [fund_id])    
    cursor.execute("UPDATE FUNDRAISING SET emailadmin=%s \
                    WHERE id=%s;", [request.session['email'],fund_id])  

    return redirect('/fundraising/page/')

def fundraising_update(request, fund_id):
    if 'email' not in request.session:
        return redirect('/')

    cursor = connection.cursor()
    cursor.execute("SELECT * FROM FUNDRAISING WHERE id=%s;", [fund_id])
    fundraising = fetch(cursor)[0]
    status = fundraising['verificationstatus']

    cursor.execute("SELECT categoryname FROM CATEGORY WHERE id=%s;", [fundraising['categoryid']])
    categoryname = fetch(cursor)[0]['categoryname']

    if ('is_admin' in request.session) or (not is_owner(request.session['email'],fund_id)) \
        or (status == 'Sudah Terverifikasi'):
        return redirect('/unauthorized/')

    form = FundraisingUpdateForm(request.POST or None, initial={
        'title':fundraising['title'],
        'description':fundraising['description'],
        'city':fundraising['city'],
        'province':fundraising['province'],
        'deadline':fundraising['lastactivedate'],
        'target':fundraising['amountoffundsneeded'],
        'repolink':fundraising['repolink']
    })

    add_form = ''
    error=''
    
    if categoryname == 'Health':
        cursor.execute("SELECT * FROM PATIENT,HEALTH_FUND WHERE fundid=%s AND patientid=nik;", [fund_id])
        patient = fetch(cursor)[0]

        cursor.execute("SELECT * FROM HEALTH_FUND HF ,COMORBID C WHERE C.fundid=%s and HF.fundid=C.fundid;", [fund_id])
        comorbid = fetch(cursor)

        if comorbid != []:
            comorbid = comorbid[0]['comorbid']
        else:
            comorbid = ''

        add_form = HealthUpdateForm(request.POST or None, initial={
            'nik': patient['nik'],
            'name':patient['name'],
            'main_disease':patient['maindisease'],
            'comorbid':comorbid
        })


    elif categoryname == 'House of Worship':
        cursor.execute("SELECT * FROM HOUSEOFWORSHIP_FUND HF, HOUSEOFWORSHIP H, CATEGORY_OF_ACTIVITY CA \
                        WHERE fundid=%s AND HF.houseofworship_id = H.certifnumb AND HF.activityid = CA.id;", [fund_id])
        how = fetch(cursor)[0]

        add_form = HoWUpdateForm(request.POST or None, initial={
            'certificate_number': how['certifnumb'],
        })

        cursor.execute("SELECT name FROM CATEGORY_OF_ACTIVITY;")
        categorynames = fetch(cursor)
        CHOICES = [(category['name'], category['name']) for category in categorynames]
        add_form.fields['activity'] = ChoiceField(label="Category Activity", choices=CHOICES)
    
    if form.is_valid() and request.POST:
        title = request.POST['title']
        desc = request.POST['description']
        city = request.POST['city']
        province = request.POST['province']
        deadline = request.POST['deadline']
        target = request.POST['target']
        repolink = request.POST['repolink']

        

    
        cursor.execute("UPDATE FUNDRAISING \
                        SET title=%s, description=%s, city=%s, province=%s, lastactivedate=%s, amountoffundsneeded=%s,repolink=%s;",
                        [title, desc, city, province, deadline, target, repolink])

        if categoryname == 'Health':
            if request.POST['nik'] != patient['nik']:
                cursor.execute("SELECT * FROM PATIENT WHERE nik=%s", [request.POST['nik']])
                new_patient = fetch(cursor)
                if new_patient == []:
                    error = "Please input a real NIK."
                    raise TypeError
                else:
                    if request.POST['name'] != new_patient[0]['name']:
                        error = "Please input an real patient."
                        raise TypeError

                    else:
                        if request.POST['main_disease'] == patient['maindisease']:
                            cursor.execute("UPDATE HEALTH_FUND SET patientid=%s WHERE fundid=%s;", [request.POST['nik'], fund_id])
                        else:
                            cursor.execute("UPDATE HEALTH_FUND SET patientid=%s, maindisease=%s WHERE fundid=%s;", [request.POST['nik'], request.POST['maindisease'], fund_id])
            
            if request.POST['name'] != patient['name']:
                error = "Please input an real patient."
                raise TypeError
            

            if request.POST['main_disease'] != patient['maindisease']:
                cursor.execute("UPDATE HEALTH_FUND SET maindisease=%s WHERE fundid=%s;", [ request.POST['main_disease'], fund_id])
            

            if 'comorbid' not in patient or request.POST['comorbid'] != patient['comorbid']:
                cursor.execute("SELECT * FROM COMORBID WHERE fundid=%s", [fund_id])
                if fetch(cursor) == []:
                    cursor.execute("INSERT INTO COMORBID (fundid,comorbid) values (%s,%s);", [fund_id, request.POST['comorbid']])
                else:
                    cursor.execute("UPDATE COMORBID SET comorbid=%s where fundid=%s;", [request.POST['comorbid'], fund_id])
                    

        elif categoryname == 'House of Worship':
            if request.POST['certificate_number'] != how['certifnumb']:
                cursor.execute("SELECT * FROM HOUSEOFWORSHIP WHERE certifnumb=%s", [request.POST['certificate_number']])
                new_how = fetch(cursor)

                if new_how != []:
                    cursor.execute("SELECT id FROM CATEGORY_OF_ACTIVITY WHERE name=%s;", [request.POST['activity']])
                    new_id = fetch(cursor)[0]['id']
                    print(how)
                    if new_id != how['activityid']:
                        cursor.execute("UPDATE HOUSEOFWORSHIP_FUND SET houseofworship_id=%s, activityid=%s WHERE fundid=%s;",
                                        [request.POST['certificate_number'], new_id, fund_id])

                    else:
                        cursor.execute("UPDATE HOUSEOFWORSHIP_FUND SET houseofworship_id=%s WHERE fundid=%s;",
                                        [request.POST['certificate_number'], fund_id])


        return redirect('/personal-fundraising/')

    

    context = {
        "form":form,
        "add_form":add_form,
        "fundraising":fundraising,
        "categoryname":categoryname,
        "error":error
    }
    return render(request, 'fundraising_update.html', context)

def fundraising_delete(request, fund_id):
    if 'email' not in request.session:
        return redirect('/')

    if ('is_admin' in request.session) or (not is_owner(request.session['email'],fund_id)):
        return redirect('/unauthorized/')

    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")
    cursor.execute("DELETE FROM FUNDRAISING WHERE emailuser=%s AND id=%s;",
                    [request.session['email'], fund_id])

    return redirect('/personal-fundraising/')

def fundraising_details(request, fund_id):
    if 'email' not in request.session:
        return redirect('/')

    if 'is_admin' in request.session:
        is_fundraiser = False
    else:
        is_fundraiser = True


    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")
    cursor.execute("SELECT F.id fundid,title,city,province,initialactivedate,lastactivedate,remainingdays, \
                    amountoffundsneeded,amountoffundscollected,amountoffundsused,categoryname,verificationstatus,repolink,emailuser \
                    FROM FUNDRAISING F, CATEGORY C \
                    WHERE F.categoryid = C.id AND F.id=%s", [fund_id])
    fundraising = fetch(cursor)[0]
    additional = ""

    if fundraising['categoryname'] == 'Health':
        cursor.execute("SELECT nik, name, maindisease, comorbid \
                        FROM COMORBID C, PATIENT P, HEALTH_FUND HF \
                        WHERE HF.fundid = %s AND C.fundid = HF.fundid \
                        AND HF.patientid = P.nik;", [fund_id])

        additional = fetch(cursor)

    elif fundraising['categoryname'] == 'House of Worship':
        cursor.execute("SELECT houseofworship_id, name activityname \
                        FROM HOUSEOFWORSHIP_FUND, CATEGORY_OF_ACTIVITY \
                        WHERE fundid=%s AND activityid = id;", [fund_id])

        additional = fetch(cursor)

    if additional == [] or additional == "":
        additional = ""
    else:
        additional = additional[0]
    
    cursor.execute("SELECT * FROM SUBMISSION_NOTES WHERE fundid=%s", [fund_id])
    notes = fetch(cursor)

    if notes == []:
        notes = ""
    
    cursor.execute("select email, timestamp, amount, prayer from donation where fundid=%s",[fund_id])
    donors = fetch(cursor)

    cursor.execute("select h.timestamp, h.nominalused, h.description from history_of_fund_use as h \
                    where h.fundid=%s",[fund_id])
    history = fetch(cursor)
    
    

    cursor.execute("SELECT email,timestamp,amount,prayer FROM DONATION where fundid=%s;",[fund_id])
    donors = fetch(cursor)
    if donors == []:
        donors = ""

    context = {
        "fundraising":fundraising,
        "notes":notes,
        "additional":additional,
        "is_fundraiser":is_fundraiser,
        "donors":donors,
        "history":history,
    }

    return render(request, 'fundraising_details.html', context)

def Manage_comorbid_diseases(request):
    return render(request, 'Manage_comorbid_diseases.html')

def comorbid_disease_form(request):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")
    cursor.execute("SELECT FundID FROM HEALTH_FUND ORDER BY FundID ASC;")

    FundID = fetch(cursor)

    CHOICES = [(health_fund['FundID'],health_fund['FundID']) for health_fund in FundID]
    form = comorbid_disease_form(request.POST or None)
    form.fields['health_fund'] = ChoiceField(label="health_fund", choices=CHOICES)

    if form.is_valid() and request.POST:
        health_fund = request.POST['health_fund']
        request.session['health_fund'] = health_fund
        return redirect('../')

    context = {
        "form": form,
    }
    return render(request, 'comorbid_disease_form.html', context)

def comorbid_disease_update(request):
    form = ComorbidUpdateForm(request.POST or None)
    context = {
        "form":form
    }
    if form.is_valid():
        return redirect('./')
    return render(request, 'comorbid_disease_update.html', context)

def fetch(cursor):
    column = [col[0] for col in cursor.description]     # cursor.description is for the names of the columns
    return [dict(zip(column, row)) for row in cursor.fetchall()]  # fetchall() to get the values of the table, result -> {'email':e, ...}

def is_owner(email, fund_id):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")
    cursor.execute("SELECT emailuser FROM FUNDRAISING WHERE id=%s;",[fund_id])

    owner_email = fetch(cursor)[0]['emailuser']

    return email == owner_email

    