from django.forms.fields import CharField, ChoiceField, DateField, FloatField, URLField
from django import forms
from django.forms.widgets import Textarea

class ComorbidForm(forms.Form):
    fundraising_id = [
        ('770600412','770600412'),
        ('770600413','770600413'),
        ('770600414','770600414'),
        ('770600415','770600415'),
        ('770600416','770600416'),
        ('770600417','770600417'),
        ('770600418','770600418'),
    ]
    id_cat = ChoiceField(label="Fundraising ID", choices= fundraising_id)
    comorbid = CharField(max_length=50)

class ComorbidUpdateForm(forms.Form):
    fundraising_id = [
        ('770600412','770600412'),
        ('770600413','770600413'),
        ('770600414','770600414'),
        ('770600415','770600415'),
        ('770600416','770600416'),
        ('770600417','770600417'),
        ('770600418','770600418'),
    ]
    id_cat = ChoiceField(label="Fundraising ID", choices= fundraising_id)
    Disease = CharField(max_length=50)

class FundraisingUpdateForm(forms.Form):
    title = CharField(widget=Textarea(   attrs={  'rows': 3,
                                                    'cols': 40,
                                                    'style':'resize:none;'
                                                    }
                                                ))
    description = CharField(widget=Textarea(   attrs={  'rows': 3,
                                                    'cols': 40,
                                                    'style':'resize:none;'
                                                    }
                                                ))
    city = CharField(max_length=50)
    province = CharField(max_length=50)
    deadline = DateField()
    target = FloatField(label="Target (rupiah)")
    repolink = URLField(label="Fundraising File")

class HealthUpdateForm(forms.Form):
    nik = CharField(label="NIK of Patient", max_length=20)
    name = CharField(max_length=50)
    main_disease = CharField(max_length=50)
    comorbid = CharField(max_length=50)

class HoWUpdateForm(forms.Form):
    certificate_number = CharField(max_length=20)
    activity = ChoiceField(label="Activity Category")

class FundraisingVerificationForm(forms.Form):
    note = CharField(label="Note (Please fill if you intend to reject)", 
                    max_length=100,
                    required=False,
                    widget=Textarea( attrs={  'rows': 3,
                                            'cols': 40,
                                            'style':'resize:none;'
                                        }
                                    ))