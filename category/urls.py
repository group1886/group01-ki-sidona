from django.urls import path

from . import views

app_name = 'category'

urlpatterns = [
    path('page/', views.category_page, name='category_page'),
    path('<str:cat_id>/update/', views.category_update, name='category_update'),
    path('add/', views.category_add, name='category_add'),
]
