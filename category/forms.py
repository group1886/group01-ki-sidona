
from django.forms.fields import CharField
from django import forms


class CategoryForm(forms.Form):
    category = CharField(max_length=50)

class CategoryUpdateForm(forms.Form):
    category = CharField(max_length=50)