from django.db import connection
from django.shortcuts import redirect, render
from .forms import CategoryForm, CategoryUpdateForm

# Create your views here.

def category_page(request):
    if 'email' not in request.session:
        return redirect('/')

    if 'is_admin' not in request.session:
        return redirect('/unauthorized/')

    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;") 
    cursor.execute("SELECT * FROM CATEGORY ORDER BY id ASC;")

    category = fetch(cursor)

    context = {
        "category":category
    }

    return render(request, 'category_page.html',context)

def category_update(request, cat_id):

    if 'email' not in request.session:
        return redirect('/')

    if 'is_admin' not in request.session:
        return redirect('/unauthorized/')

    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;") 
    cursor.execute("SELECT * FROM CATEGORY WHERE id=%s;", [cat_id])

    category = fetch(cursor)[0]

    form = CategoryUpdateForm(request.POST or None, initial={'category':category['categoryname']})

    if form.is_valid() and request.POST:
        new_category = request.POST['category']
        cursor.execute("UPDATE CATEGORY SET categoryname=%s WHERE id=%s;", [new_category, cat_id])
        return redirect('/category/page/')
        

    context = {
        "form":form,
        "category":category
    }

    return render(request, 'category_update.html', context)

def category_add(request):

    if 'email' not in request.session:
        return redirect('/')

    if 'is_admin' not in request.session:
        return redirect('/unauthorized/')

    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;") 
    cursor.execute("SELECT MAX(ID) max FROM CATEGORY;")

    last_id = fetch(cursor)[0]['max']
    new_id = int(last_id) + 1

    form = CategoryForm(request.POST or None)

    if form.is_valid():
        new_category = request.POST['category']
        cursor.execute("INSERT INTO CATEGORY (id,categoryname) values (%s,%s)",[new_id, new_category])
        return redirect('/category/page/')

    context = {
        "form":form,
        "new_id":new_id
    }
    return render(request, 'category_add.html', context)


def fetch(cursor):
    column = [col[0] for col in cursor.description]     # cursor.description is for the names of the columns
    return [dict(zip(column, row)) for row in cursor.fetchall()]  # fetchall() to get the values of the table, result -> {'email':e, ...}