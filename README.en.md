

GROUP01 INTERNATIONAL CLASS - SIDONA CASE STUDY


## Table of contents

- [Table of contents](#table-of-contents)
- [Changes made](#changes-made)

## Changes made
- Changed the initial empid values from Individual Assignment 3 queries to satisfy the requirement that every empid is +1 the previous empid
- Changed the initial fundid values to have the format given: 'Initial-Number'
- Changed the category_id values in table CATEGORY to fit requirements of using 1,2,3,etc.
