from django.utils import timezone
from django.forms.fields import ChoiceField
from .forms import AdditionalHealthForm, AdditionalHoWForm, FundraisingCategoryForm, FundraisingForm, HouseOfWorshipForm, PatientForm, RegisteredHoWForm, RegisteredPatientForm
from django.shortcuts import redirect, render
from django.db import connection



def choose_category(request):
    if 'email' not in request.session:
        return redirect('/')

    if 'is_admin' in request.session:
        return redirect('/unauthorized/')

    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")
    cursor.execute("SELECT categoryname FROM CATEGORY ORDER BY ID ASC;")

    categorynames = fetch(cursor)

    CHOICES = [(category['categoryname'],category['categoryname']) for category in categorynames]

    form = FundraisingCategoryForm(request.POST or None)
    form.fields['category'] = ChoiceField(label="Category", choices=CHOICES)

    if form.is_valid() and request.POST:
        category = request.POST['category']
        request.session['category'] = category
        if category == 'Health':
            return redirect('/fundraising/create/patient-choose/')
        elif category == 'House of Worship':
            return redirect('/fundraising/create/houseofworship-choose/')
        else:
            return redirect('/fundraising/create/fundraising-form/')

    context = {
        "form": form,
    }

    return render(request, 'fundraising_category_form.html', context)

def fundraising_form(request):

    if 'email' not in request.session:
        return redirect('/')

    if 'is_admin' in request.session:
        return redirect('/unauthorized/')

    form = FundraisingForm(request.POST or None)
    error =''
    category = request.session.get('category')
    initial = category[0]
    
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")
    cursor.execute("SELECT id FROM CATEGORY WHERE categoryname=%s;", [category])
    c_id = fetch(cursor)[0]['id']

    cursor.execute("SELECT COUNT(ID) LAST FROM FUNDRAISING WHERE categoryid=%s;",[c_id])
    order = int(fetch(cursor)[0]['last']) + 1

    fundid = f"{initial}-{str(order).zfill(3)}"

    if category == 'Health':
        add_info = request.session['patient']
        add_form = AdditionalHealthForm(request.POST or None)
        

    elif category == 'House of Worship':
        add_info = request.session['how']
        add_form = AdditionalHoWForm(request.POST or None)
        
    else:
        add_form = ''

    if form.is_valid() and request.POST:
        title = request.POST['title']
        desc = request.POST['description']
        city = request.POST['city']
        province = request.POST['province']
        deadline = request.POST['deadline']
        target = request.POST['target']
        repolink = request.POST['repolink']

        cursor = connection.cursor()
        cursor.execute("SET SEARCH_PATH TO SIDONA;")
        cursor.execute("INSERT INTO FUNDRAISING (id,title,description,city,province,repolink,verificationstatus,submitdate,\
                        initialactivedate,lastactivedate,amountoffundsneeded,amountoffundscollected,amountoffundsused,remainingdays,\
                        emailuser,emailadmin,categoryid) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);",
                        [fundid, title, desc, city, province, repolink, 'Belum Terverifikasi', timezone.localtime(timezone.now()),
                        None,deadline,target,None,None,None,request.session['email'],None,c_id])

        if category == 'Health' and add_form.is_valid():
            main_disease = request.POST['main_disease']
            cursor.execute("INSERT INTO HEALTH_FUND (fundid, maindisease, patientid) values (%s,%s,%s);", 
                            [fundid, main_disease, request.session['patient']['nik']])

        elif category == 'House of Worship':
            activity = request.POST['activity']
            cursor.execute("INSERT INTO HOUSEOFWORSHIP_FUND (fundid, houseofworship_id, activityid) values (%s,%s,%s);", 
                            [fundid, request.session['how']['certifnumb'], activity])

        return redirect('/personal-fundraising/')

    else:
        error = "There's something wrong, please input again."


    context = {
        "form":form,
        "category":category,
        "fundid":fundid,
        "add_form":add_form,
        "add_info":add_info,
        "error":error
    }
    return render(request, 'fundraising_form.html', context)


def patient_choose(request):
    return render(request, 'patient_choose.html')

def patient_form(request):
    form = PatientForm(request.POST or None)
    error = ''

    if form.is_valid() and request.POST:

        nik = request.POST['nik']
        name = request.POST['name']
        birthdate = request.POST['dob']
        address = request.POST['address']
        job = request.POST['job']

        try:
            cursor = connection.cursor()
            cursor.execute("SET SEARCH_PATH TO SIDONA;")
            cursor.execute("INSERT INTO PATIENT (nik,name,birthdate,address,job) values (%s,%s,%s,%s,%s);", [nik,name,birthdate,address,job])
            patient = {"nik":nik, "name":name, "birthdate":str(birthdate), "address":address, "job":job}
            request.session['patient'] = patient
            return redirect('/fundraising/create/fundraising-form/')

        except:
            error = "Try again, make sure the NIK has not been registed before."

        
    context = {
        "form":form,
        "error": error,
    }

    return render(request, 'patient_form.html', context)

def registered_patients(request):
    form = RegisteredPatientForm(request.POST or None)
    error_message = ""
    
    if form.is_valid():
        nik = request.POST['nik']
        
        cursor = connection.cursor()
        cursor.execute("SET SEARCH_PATH TO SIDONA;")
        cursor.execute("SELECT * FROM PATIENT WHERE nik=%s;", [nik])

        patient = fetch(cursor)

        if patient != []:
            patient = patient[0]
            patient['birthdate'] = str(patient['birthdate'])
            request.session['patient'] = patient
            return redirect('/fundraising/create/found-patient/')
        
        else:
            error_message = "NIK is not valid. Please try again."

    context = {
        "form":form,
        "error":error_message
    }

    return render(request, 'registered_patients.html', context)

def found_patient(request):

    context = {
        "patient":request.session['patient']
    }

    return render(request, 'found_patient.html', context)


def how_choose(request):
    return render(request, 'houseofworship_choose.html')

def how_form(request):
    form = HouseOfWorshipForm(request.POST or None)
    error = ''

    if form.is_valid() and request.POST:

        certifnumb = request.POST['certificate_number']
        name = request.POST['name']
        address = request.POST['address']
        type = request.POST['type']

        try:
            cursor = connection.cursor()
            cursor.execute("SET SEARCH_PATH TO SIDONA;")
            cursor.execute("INSERT INTO HOUSEOFWORSHIP (certifnumb,name,address,type) values (%s,%s,%s,%s);", [certifnumb,name,address,type])
            how = {"certifnumb":certifnumb, "name":name, "address":address, "type":type}
            request.session['how'] = how
            return redirect('/fundraising/create/fundraising-form/')

        except:
            error = "Try again, make sure the Certificate Number has not been registed before."

    context = {
        "form":form,
        "error": error,
    }

    return render(request, 'houseofworship_form.html', context)

def registered_hows(request):
    form = RegisteredHoWForm(request.POST or None)
    error_message = ""
    
    if form.is_valid():
        certifnumb = request.POST['certificate_number']
        
        cursor = connection.cursor()
        cursor.execute("SET SEARCH_PATH TO SIDONA;")
        cursor.execute("SELECT * FROM HOUSEOFWORSHIP WHERE certifnumb=%s;", [certifnumb])

        how = fetch(cursor)

        if how != []:
            how = how[0]
            request.session['how'] = how
            return redirect('/fundraising/create/found-how/')
        
        else:
            error_message = "Certificate Number is not valid. Please try again."

    context = {
        "form":form,
        "error":error_message
    }

    return render(request, 'registered_houseofworship.html', context)

def found_how(request):

    context = {
        "how":request.session['how']
    }

    return render(request, 'found_how.html', context)

def fetch(cursor):
    column = [col[0] for col in cursor.description]     # cursor.description is for the names of the columns
    return [dict(zip(column, row)) for row in cursor.fetchall()]  # fetchall() to get the values of the table, result -> {'email':e, ...}

