from django.forms.fields import CharField, ChoiceField, DateField, FloatField, URLField
from django import forms
from django.forms.widgets import Textarea



class FundraisingCategoryForm(forms.Form):
    CHOICES = [
        ('Health','Health'),
        ('House of Worship','House of Worship'),
        ('Education','Education'),
        ('Disaster','Disaster'),
        ('Social','Social'),
    ]
    category = ChoiceField(label="Category", choices=CHOICES)

class FundraisingForm(forms.Form):
    title = CharField(widget=forms.Textarea(attrs={  'rows': 3,
                                                    'cols': 40,
                                                    'style':'resize:none;'
                                                }))
    description = CharField(widget=forms.Textarea(attrs={  'rows': 3,
                                                    'cols': 40,
                                                    'style':'resize:none;'
                                                }))
    city = CharField(max_length=50)
    province = CharField(max_length=50)
    deadline = DateField()
    target = FloatField(label="Target (rupiah)")
    repolink = URLField(label="Fundraising File")
    
class AdditionalHealthForm(forms.Form):
    main_disease = CharField(max_length=50)

class AdditionalHoWForm(forms.Form):
    activity = ChoiceField(label="Activity Category")

class PatientForm(forms.Form):
    nik = CharField(label="NIK", max_length=20)
    name = CharField(max_length=50)
    dob = DateField()
    address = CharField(max_length=50)
    job = CharField(max_length=50)

class RegisteredPatientForm(forms.Form):
    nik = CharField(label="NIK", max_length=20) 

class HouseOfWorshipForm(forms.Form):
    certificate_number = CharField(max_length=20)
    name = CharField(max_length=50)
    address = CharField(max_length=50)
    type = CharField(max_length=50)

class RegisteredHoWForm(forms.Form):
    nik = CharField(label="Certificate Number", max_length=20) 


