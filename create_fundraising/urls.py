
from django.urls import path
from . import views

app_name = 'create_fundraising'

urlpatterns = [
    path('choose-category/', views.choose_category, name='choose_category'),
    path('fundraising-form/', views.fundraising_form, name='fundraising_form'),

    path('patient-form/', views.patient_form, name='patient_form'),
    path('patient-choose/', views.patient_choose, name='patient_choose'),
    path('registered-patients/', views.registered_patients, name='registered_patients'),
    path('found-patient/', views.found_patient, name="found_patient"),
    
    path('houseofworship-choose/', views.how_choose, name='how_choose'),
    path('registered-houseofworships/', views.registered_hows, name='registered_hows'),
    path('houseofworship-form/', views.how_form, name='how_form'),
    path('found-how/', views.found_how, name="found_how"),
]




    