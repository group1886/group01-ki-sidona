from django.shortcuts import redirect, render
from django.db import connection
from .forms import AdminForm, FundraiserForm, IndividualForm, LoginForm, OrganizationForm

# Create your views here.

def login(request):
    if 'email' in request.session:
        return redirect('/dashboard/')

    context = dict()
    context['error'] = ''

    form = LoginForm(request.POST or None)
    context['form'] = form
    if(request.method == "POST" and form.is_valid()):
        context['email'] = request.POST['email']
        context['password'] = request.POST['password']
        if(is_verified(context)):
            request.session['email'] = context['email']

            username = ''
            for i in context['email']:
                if i != '@':
                    username += i
                else:
                    break
            request.session['username'] = username

            if(is_admin(context['email'])):
                request.session['is_admin'] = True
            elif(is_individual(context['email'])):
                request.session['is_individual'] = True
            else:
                request.session['is_organization']= True

            return redirect('/dashboard/')
        else:
            context['error'] = 'The email or password is incorrect, please try again.'

    return render(request, 'login.html', context)

def register(request):
    return render(request, 'register.html')

def admin(request):
        
    form = AdminForm(request.POST or None)
    context = {
        "form": form,
        "error": ""
    }

    if form.is_valid() and request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        name = request.POST['name']
        phone_number = request.POST['phone_number']
        
        try:
            insert_admin(email,password,name,phone_number)
            request.session.flush()
            return redirect('/login/')  
        except:
            context['error'] = "Email is already registered. Please enter a new email."

    return render(request, 'register_admin.html', context)

def fundraiser(request):

    form = FundraiserForm(request.POST or None)

    if 'email' in request.session:
        form = FundraiserForm(request.POST or None, initial={
            'email': request.session['email'],
            'password':request.session['password'],
            'name': request.session['name'],
            'phone_number':request.session['phone_number'],
            'address': request.session['address'],
            'bank_name':request.session['bank_name'],
            'account_number': request.session['account_number'],
            'type':request.session['type'],
        })
    
    if form.is_valid() and request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        name = request.POST['name']
        phone_number = request.POST['phone_number']
        address = request.POST['address']
        bank_name = request.POST['bank_name']
        account_number = request.POST['account_number']
        type = request.POST['type']

        request.session['email'] = email
        request.session['password'] = password
        request.session['name'] = name
        request.session['phone_number'] = phone_number
        request.session['address'] = address
        request.session['bank_name'] = bank_name
        request.session['account_number'] = account_number
        request.session['type'] = type

        if type == "Individual":
            return redirect('/register/fundraiser/individual')
        else:
            return redirect('/register/fundraiser/organization')

    context = {
        "form": form,
    }
    return render(request, 'register_fundraiser.html', context)

def individual(request):

    form = IndividualForm(request.POST or None)
    error = ''

    if 'nik' in request.session:
        form = IndividualForm(request.POST or None, initial={
            'nik': request.session['nik'],
            'dob': request.session['dob'],
            'gender': request.session['gender'],
            'id_photo':request.session['id_photo'],
        })

    if form.is_valid() and request.POST:
        nik = request.POST['nik']
        dob = request.POST['dob']
        gender = request.POST['gender']
        id_photo = request.POST['id_photo']

        email = request.session['email']
        password = request.session['password']
        name = request.session['name']
        phone_number = request.session['phone_number']
        address = request.session['address']
        bank_name = request.session['bank_name']
        account_number = request.session['account_number']

        request.session['nik'] = nik
        request.session['dob'] = dob
        request.session['gender'] = gender
        request.session['id_photo'] = id_photo

        try:
            insert_fundraiser(email,password,name,phone_number,address, 
                                    0.0, account_number, bank_name, id_photo, 0, 0, 'Belum verifikasi')
            insert_individual(email, nik, dob, gender, 0)
            request.session.flush()
            return redirect('/login/')
        
        except:
            error = "Email is already registered. Please enter a new email."

    context = {
        "form": form,
        "error": error
    }

    return render(request, 'register_individual.html', context)

def organization(request):

    form = OrganizationForm(request.POST or None)
    error = ''

    if form.is_valid() and request.POST:
        organization_name = request.POST['organization_name']
        deed_of_est = request.POST['deed_of_est']
        organization_phone = request.POST['organization_phone']
        year_of_est = request.POST['year_of_est']
        photo_of_deed = request.POST['photo_of_deed']

        email = request.session['email']
        password = request.session['password']
        name = request.session['name']
        phone_number = request.session['phone_number']
        address = request.session['address']
        bank_name = request.session['bank_name']
        account_number = request.session['account_number']

        
        try:
            insert_fundraiser(email,password,name,phone_number,address, 
                                0.0, account_number, bank_name, photo_of_deed, 0, 0, 'Belum verifikasi')
            insert_organization(email, deed_of_est, organization_name, organization_phone, year_of_est)
            request.session.flush()
            return redirect('/login/')
        except:
            error = "Email is already registered. Please enter a new email."
            

    context = {
        "form": form,
        "error":error
    }

    return render(request, 'register_organization.html', context)

def is_verified(data):
    
    admin_query = """SELECT * FROM admin;"""

    fundraiser_query = "SELECT * FROM fundraiser;"

    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")
    cursor.execute(admin_query)
    admin_list = fetch(cursor)

    cursor.execute(fundraiser_query)
    fundraiser_list = fetch(cursor)

    for admin in admin_list:
        if (data['email'] == admin['email'] and 
            data['password'] == admin['password']):
            return True

    for fundr in fundraiser_list:
        if (data['email'] == fundr['email'] and 
            data['password'] == fundr['password']):
            return True

    return False
    
def is_admin(email):
    admin_query = """SELECT email FROM admin;"""

    # run query
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")
    cursor.execute(admin_query)
    admin_list = fetch(cursor)

    # go through emails
    for admin in admin_list:
        if (email == admin['email']):
            return True
    return False

def is_individual(email):
    indi_query = """SELECT email FROM individual;"""

    # run query
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")
    cursor.execute(indi_query)
    indi_list = fetch(cursor)
    
    # go through emails
    for indi in indi_list:
        if (email == indi['email']):
            return True
    return False

def is_organization(email):
    indi_query = """SELECT email FROM organization;"""

    # run query
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")
    cursor.execute(indi_query)
    org_list = fetch(cursor)

    # go through emails
    for org in org_list:
        if (email == org['email']):
            return True
    return False

def insert_admin(email,password,name,phone_number):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")

    cursor.execute("SELECT MAX(EMPID) AS MAX FROM ADMIN;")  # get last emp_id generated
    last_empid = fetch(cursor)[0]['max']
    cursor.execute("INSERT INTO ADMIN (email,password,name,phonenumber,empid) values (%s,%s,%s,%s,%s)",[email,password,name,phone_number,(int(last_empid) + 1)])
    
def insert_fundraiser(email,password,name,phone_number, address, donapaysaldo, account_number, bank_name, id_photo, numboffund, numbofactivefund, verificationstatus):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")
    cursor.execute("INSERT INTO FUNDRAISER (email,password,name,phonenumber,address,donapaysaldo,accountnumb,bankname,repolink,numboffund,numb_of_active_fund,verificationstatus) \
                    values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);",
                    [email,password,name,phone_number, address, donapaysaldo, account_number, bank_name, id_photo, numboffund, numbofactivefund, verificationstatus])

def insert_individual(email, nik, dob, gender, numofwishlist):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")
    cursor.execute("INSERT INTO INDIVIDUAL (email,nik,birthdate,gender,numberofwishlist) values (%s,%s,%s,%s,%s)",[email,nik,dob,gender,numofwishlist])
    
def insert_organization(email, deed_of_est, organization_name, organization_phone, year_of_est):
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")
    cursor.execute("INSERT INTO ORGANIZATION (email,deednumber,organizationname,telpnumb,establishmentyear) values (%s,%s,%s,%s,%s)",[email,deed_of_est,organization_name,organization_phone,year_of_est])

def unauthorized(request):
    if "email" in request.session:
        return render(request,'unauthorized.html')
    else:
        return redirect('/')

def logout(request):
    if "email" in request.session:
        request.session.flush()
        return redirect('/')
    return redirect('/')

def fetch(cursor):
    column = [col[0] for col in cursor.description]     # cursor.description is for the names of the columns
    return [dict(zip(column, row)) for row in cursor.fetchall()]  # fetchall() to get the values of the table, result -> {'email':e, ...}

