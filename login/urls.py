from django.urls import path

from . import views

app_name = 'login'

urlpatterns = [
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('register/', views.register, name='register'),
    path('register/admin/', views.admin, name='register_admin'),
    path('register/fundraiser/', views.fundraiser, name='register_fundraiser'),
    path('register/fundraiser/individual', views.individual, name='register_individual'),
    path('register/fundraiser/organization', views.organization, name='register_organization'),
    path('unauthorized/', views.unauthorized, name='unauthorized'),
]
