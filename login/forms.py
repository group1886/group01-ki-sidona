
from django.forms.fields import ChoiceField, DateField, EmailField, CharField, IntegerField, URLField
from django import forms


class AdminForm(forms.Form):
    email = EmailField(max_length=50)
    password = CharField(max_length=50, widget=forms.PasswordInput())
    name = CharField(max_length=50)
    phone_number = CharField(max_length=20)

class LoginForm(forms.Form):
    email = EmailField(max_length=50)
    password = CharField(max_length=50, widget=forms.PasswordInput())

class FundraiserForm(forms.Form):
    CHOICES = [
        ('Individual','Individual'),
        ('Organization','Organization'),
    ]
    email = EmailField(max_length=50)
    password = CharField(max_length=50, widget=forms.PasswordInput())
    name = CharField(max_length=50)
    phone_number = CharField(max_length=20)
    address = CharField(widget=forms.Textarea(attrs={  'rows': 3,
                                                    'cols': 40,
                                                    'style':'resize:none;'
                                                }))
    bank_name = CharField(max_length=20)
    account_number = CharField(max_length=20)
    type = ChoiceField(choices=CHOICES)


class IndividualForm(forms.Form):
    nik = CharField(max_length=20)
    dob = DateField()
    gender = CharField(max_length=1, label='Gender (M/F)')
    id_photo = URLField(label='Photo of ID card')

class OrganizationForm(forms.Form):
    organization_name = CharField(max_length=20)
    deed_of_est = CharField(max_length=50, label='Deed of Establishment Number')
    organization_phone = CharField(max_length=50)
    year_of_est = IntegerField(label='Year of Establishment')
    photo_of_deed = URLField(max_length=200, label='Photo of Deed of Establishment (URL)')