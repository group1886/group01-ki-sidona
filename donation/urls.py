from django.urls import path

from.import views

app_name = 'donation'

urlpatterns = [
    
    path('fundraising_donation_data/', views.f_donation, name='f_donation_data'),
    path('donation_status/', views.d_status, name='donation_status'),
    path('donation_form/<str:fund_id>/', views.d_form, name='donation_form'),
    path('upload_proof/', views.upload_proof, name='upload_proof'),
    path('wishlist/delete/<str:fund_id>/', views.wishlist_delete, name='wishlist_delete'),

]