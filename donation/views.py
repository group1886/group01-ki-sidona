from datetime import time
from django.shortcuts import render, redirect
from django.db import connection
from django.utils import timezone
from donation.forms import DonationForm


def f_donation(request):
    if 'email' not in request.session:
        return redirect('/')
    if 'is_individual' not in request.session:
        return redirect('/unauthorized/')
    
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")
    cursor.execute("SELECT D.fundid, F.title, C.categoryname, P.status \
                    FROM DONATION D, FUNDRAISING F, CATEGORY C, PAYMENT_STATUS P \
                    WHERE D.email=%s AND F.id=D.fundid AND F.categoryid=C.id \
                    AND D.paymentstatusid=P.id;", [request.session['email']])

    donation = fetch(cursor)

    if donation == []:
        donation = ""
    
    context = {
        "donation":donation
    }

    return render(request, 'fundraising_donation_data.html', context)

def d_status(request):
    if 'email' not in request.session:
        return redirect('/')
    if 'is_individual' not in request.session:
        return redirect('/unauthorized/')
        
    return render(request, 'donation_status.html')

def d_form(request, fund_id):
    if 'email' not in request.session:
        return redirect('/')
    if 'is_individual' not in request.session:
        return redirect('/unauthorized/')
    
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")
    cursor.execute("SELECT title FROM FUNDRAISING WHERE id=%s;", [fund_id])
    fund_title = fetch(cursor)[0]['title']

    form = DonationForm(request.POST or None)
    timestamp = timezone.localtime(timezone.now())

    if form.is_valid() and request.POST:
        email = request.session['email']
        nominal = request.POST['nominal']
        payment = request.POST['payment']
        anonymous_status = request.POST['anonymous_status']
        prayer = request.POST['prayer']
        

        cursor.execute("INSERT INTO DONATION (email,timestamp,amount,paymentmethod,anonymousstatus,prayer,fundid,paymentstatusid) \
                        values (%s,%s,%s,%s,%s,%s,%s,%s)", [email,timestamp,nominal,payment,anonymous_status,prayer,fund_id,'924491011'])
        return redirect('/donation/fundraising_donation_data/')

    context = {
        'form': form,
        'fund_title':fund_title,
        'timestamp': timestamp
        
    }
    return render(request, 'donation_form.html', context)

def wishlist_delete(request, fund_id):
    if 'email' not in request.session:
        return redirect('/')
    if 'is_individual' not in request.session:
        return redirect('/unauthorized/')

    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIDONA;")
    cursor.execute("DELETE FROM WISHLIST_DONATION WHERE fundid=%s AND email=%s;", [fund_id, request.session['email']])
    
    return redirect('/profile/')

def fetch(cursor):
    column = [col[0] for col in cursor.description]     # cursor.description is for the names of the columns
    return [dict(zip(column, row)) for row in cursor.fetchall()]  # fetchall() to get the values of the table, result -> {'email':e, ...}

def upload_proof(request):
    return render(request, 'UploadPaymentProof.html')
