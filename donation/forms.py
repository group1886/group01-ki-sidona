from django.forms.fields import CharField, ChoiceField, DateField, FloatField
from django import forms
from django.forms.widgets import Textarea

class DonationForm(forms.Form):
    nominal = FloatField(label="Nominal (rupiah)")
    choices_payment = [
        ('Transfer', 'Transfer'),
        ('DonaPay','DonaPay'),
    ]
    payment = ChoiceField(label="Payment", choices=choices_payment)
    choices_anonymous = [
        ('Yes','Yes'),
        ('No','No'),
    ]
    anonymous_status = ChoiceField(label="Anonymous_status", choices=choices_anonymous)
    prayer = CharField(max_length=50)